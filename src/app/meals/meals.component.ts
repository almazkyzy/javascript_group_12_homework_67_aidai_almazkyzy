import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Meal } from '../shared/meal.model';
import { MealService } from '../shared/meal.service';

@Component({
  selector: 'app-meals',
  templateUrl: './meals.component.html',
  styleUrls: ['./meals.component.css']
})
export class MealsComponent implements OnInit, OnDestroy {
  meals: Meal[] = [];
  mealsFetching = false;
  mealsChangeSubscription!: Subscription;
  mealsFetchingSubscription!: Subscription;

  constructor(private mealService: MealService) { }

  ngOnInit(): void {
    this.meals = this.mealService.getMeals();
    this.mealsChangeSubscription = this.mealService.mealsChange.subscribe((meals: Meal[]) => {
      this.meals = meals;
    });
    this.mealsFetchingSubscription = this.mealService.mealsFetching.subscribe(isFetching => {
      this.mealsFetching = isFetching;
    });
    this.mealService.fetchMeals();
  }
  ngOnDestroy() {
    this.mealsChangeSubscription.unsubscribe();
    this.mealsFetchingSubscription.unsubscribe();
  }

}
