import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Meal } from '../shared/meal.model';
import { MealService } from '../shared/meal.service';
import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';


export class MealResolverService implements Resolve<Meal> {
  constructor(private mealService: MealService,
              private router: Router) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Meal> | Observable<never> {
    const id = <string>route.params['id'];
    return this.mealService.fetchMeal(id).pipe(mergeMap(meal => {
      if (meal) {
        return of(meal);
      } else {
        void this.router.navigate(['/meals']);
        return EMPTY;
      }
    }));
  }
}
