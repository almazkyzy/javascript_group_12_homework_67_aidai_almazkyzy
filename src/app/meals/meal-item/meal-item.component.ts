import { Component, Input, OnInit } from '@angular/core';
import { MealService } from '../../shared/meal.service';
import { Meal } from '../../shared/meal.model';

@Component({
  selector: 'app-meal-item',
  templateUrl: './meal-item.component.html',
  styleUrls: ['./meal-item.component.css']
})
export class MealItemComponent implements OnInit {
  @Input() meal!: Meal;

  constructor(private mealService: MealService) { }

  ngOnInit(): void {
  }

}
