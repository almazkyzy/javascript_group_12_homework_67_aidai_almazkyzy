import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';
import { MealService } from '../shared/meal.service';
import { Meal } from '../shared/meal.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-new-meal',
  templateUrl: './new-meal.component.html',
  styleUrls: ['./new-meal.component.css']
})
export class NewMealComponent implements OnInit, OnDestroy {
  @ViewChild('f') form!: NgForm;
  isEdit = false;
  editedId = '';
  intakeTimes = [
    {name: 'Breakfast'},
    {name: 'Snack'},
    {name: 'Lunch'},
    {name: 'Dinner'},
  ];

  mealUploadingSubscription!: Subscription;
  mealUploading = false;

  constructor(public mealService: MealService,
              private router: Router,
              private route: ActivatedRoute,) { }

  ngOnInit() {
    this.mealUploadingSubscription = this.mealService.mealUploading.subscribe(isLoading => {
      this.mealUploading = isLoading;
    });
    this.route.data.subscribe(data => {
      const meal = <Meal | null>data.meal;
      if (meal) {
        this.isEdit = true;
        this.editedId = meal.id;
        this.setFormValue({
          intakeTime:meal.intakeTime,
          description: meal.description,
          calories:meal.calories
        });
      } else {
        this.isEdit = false;
        this.editedId = '';
        this.setFormValue({
          intakeTime: '',
          description: '',
          calories: '',
        })
      }
    });
  }

  ngOnDestroy() {
    this.mealUploadingSubscription.unsubscribe();
  }

  setFormValue(value: {[key: string]: any}) {
    setTimeout(() => {
      this.form.form.setValue(value);
    });
  }

  saveMeal() {
    const id = this.editedId || Math.random().toString();
    const meal = new Meal(
      id,
      this.form.value.intakeTime,
      this.form.value.description,
      this.form.value.calories,
    );
    const next = () => {
      this.mealService.fetchMeals();
      void this.router.navigate(['..'], {relativeTo: this.route});
    };
    if (this.isEdit) {
      this.mealService.editMeal(meal).subscribe(next);
    } else {
      this.mealService.addMeal(meal).subscribe(next);
    }
  }

}
