import { Subject } from 'rxjs';
import { Meal } from './meal.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';

@Injectable()

export class MealService {
  mealsChange = new Subject<Meal[]>();
  mealsFetching = new Subject<boolean>();
  mealUploading = new Subject<boolean>();
  mealRemoving = new Subject<boolean>();


  private meals: Meal[] = [];

  constructor(private http: HttpClient) {
  }

  fetchMeals() {
    this.mealsFetching.next(true);
    this.http.get<{[id:string]: Meal}>('https://aidai-fa920-default-rtdb.firebaseio.com/meals.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const mealData = result[id];
          const meal = new Meal(
            id,
            mealData.intakeTime,
            mealData.description,
            mealData.calories
          );
          return meal;
        });
      }))
      .subscribe(meals => {
        this.meals = meals;
        this.mealsChange.next(this.meals.slice());
        this.mealsFetching.next(false);
      }, error => {
        this.mealsFetching.next(false);
      });
  }

  getMeals(){
    return this.meals.slice();
  }

  addMeal(meal: Meal) {
    const body = {
      intakeTime: meal.intakeTime,
      description: meal.description,
      calories: meal.calories,
    };
    this.mealUploading.next(true);

    return this.http.post('https://aidai-fa920-default-rtdb.firebaseio.com/meals.json', body).pipe(
      tap(() => {
        this.mealUploading.next(false);
      }, () => {
        this.mealUploading.next(false);
      })
    );
  }
  fetchMeal(id: string) {
    return this.http.get<Meal | null>(`https://aidai-fa920-default-rtdb.firebaseio.com/meal/${id}.json`).pipe(
      map(result => {
        if (!result) return null;

        return new Meal(
          id, result.intakeTime, result.description,
          result.calories
        );
      })
    );
  }

  removeMeal(id: string) {
    this.mealRemoving.next(true);
    return this.http.delete(`https://aidai-fa920-default-rtdb.firebaseio.com/meals/${id}.json`).pipe(
      tap(() => {
        this.mealRemoving.next(false);
      }, () => {
        this.mealRemoving.next(false);
      })
    );
  };

  editMeal(meal: Meal) {
    const body = {
      intakeTime: meal.intakeTime,
      description: meal.description,
      calories: meal.calories,
    };
    this.mealUploading.next(true);
    return this.http.put(`https://aidai-fa920-default-rtdb.firebaseio.com/meals/${meal.id}.json`, body).pipe(
      tap(() => {
        this.mealUploading.next(false);
      }, () => {
        this.mealUploading.next(false);
      })
    );
  }




}
