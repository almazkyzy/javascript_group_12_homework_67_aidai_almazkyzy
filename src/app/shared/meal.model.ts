export class Meal {
  constructor(
    public id: string,
    public intakeTime: string,
    public description: string,
    public calories: number
  ) {}
}
