import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MealsComponent } from './meals/meals.component';
import { MealItemComponent } from './meals/meal-item/meal-item.component';
import { MealService } from './shared/meal.service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NewMealComponent } from './new-meal/new-meal.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MealsComponent,
    MealItemComponent,
    NewMealComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [MealService],
  bootstrap: [AppComponent]
})
export class AppModule { }
