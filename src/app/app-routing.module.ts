import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MealsComponent } from './meals/meals.component';
import { NewMealComponent } from './new-meal/new-meal.component';

const routes: Routes = [
  {
    path: '',
    component: MealsComponent},
  {
    path: 'meals',
    component: MealsComponent,
    children: [
      {
        path: 'new',
        component: NewMealComponent},
      {
        path: ':id/edit',
        component: NewMealComponent,
        resolve: {meal: MealResolverService}
      },
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
